﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LearningCurve : MonoBehaviour
{
    public int numberOne = 4;
    public int numberTwo = 5;
    public float pi = 3.14f;
    public int piConvertedInt;
    public string characterName = "Luca";
    public int characterLevel = 10;
    public bool allGood = true;
    public bool pureOfHeart = true;
    public bool hasSecretIncantation = false;
    public string rareItem = "1911";

    public int currentGold = 0;

    // Start is called before the first frame update
    void Start()
    {
        OpenTresureChamber();
        piConvertedInt = (int)pi;

        //Debug.Log(AddNumber());
        Debug.LogFormat("I added {0} + {1} which is {2}", numberOne, numberTwo, AddNumber());

        //Debug.Log($"A string can have a varibale: {numberOne}");
        //Debug.Log("A string can have a varibale:" + numberTwo);

        

       //Debug.Log(piConvertedInt);
        int nextSkillLevel = GenerateCharacter(characterName, characterLevel);
        Debug.Log(nextSkillLevel);
        Debug.Log(GenerateCharacter(characterName, characterLevel));

        Test(GenerateCharacter(characterName, characterLevel));

        if(currentGold > 50)
        {
            Debug.Log("You have more then 50 Gold");
        }
        else if(currentGold < 15)
        {
            Debug.Log("You have less then 15 Gold");
        }
        else
        {
            Debug.Log("You have between 15 and 50 Gold");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    int AddNumber()
    {
        return numberOne + numberTwo;
    }

    public int GenerateCharacter(string characterName, int characterLevel)
    {
        //Debug.LogFormat("Character: {0} - Level: {1}", characterName, characterLevel);
        return characterLevel +5;
        //Debug.Log($"Character: {characterName} - Level: {characterLevel}");
    }

    public void Test(int test)
    {
        Debug.Log(test);
    }
    public void OpenTresureChamber()
    {
        if(pureOfHeart && rareItem == "1911")
        {
            Debug.Log("1 IF");
            if(!hasSecretIncantation)
            {
                Debug.Log("Not worthy");
            }
            else
            {
                Debug.Log("Worthy");
            }
        }
        else
        {
            Debug.Log("Else");
        }
    }
}
